## API Fuzzing

This project contains resources for API Fuzzing. The container registry hosts the API Fuzzing docker images pulled by the API Fuzzing template. Starter configuration files are provided in the repository.

`gitlab-api-fuzzing-config.yml` -- Copy this file into your registry as `.gitlab-api-fuzzing.yml`. This file has a basic configuration suitable for any web API technology.
